Source: quantlib
Section: libs
Priority: optional
Maintainer: Dirk Eddelbuettel <edd@debian.org>
Build-Depends: debhelper-compat (= 13), autoconf, automake, texinfo, libboost-dev, libboost-test-dev, libboost-timer-dev, libboost-system-dev, g++ (>= 4:5.2)
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/edd/quantlib
Vcs-Git: https://salsa.debian.org/edd/quantlib.git
Homepage: https://www.quantlib.org

Package: libquantlib0v5
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks:  libquantlib-1.1, libquantlib-1.0.0
Replaces: libquantlib-1.2, libquantlib-1.1, libquantlib-1.0.0, libquantlib0
Conflicts: libquantlib0
Provides: libquantlib-1.2, libquantlib-1.1, libquantlib-1.0.0
Description: Quantitative Finance Library -- library package
 The QuantLib project aims to provide a comprehensive software framework 
 for quantitative finance. The goal is to provide a standard free/open 
 source library to quantitative analysts and developers for modeling, 
 trading, and risk management of financial assets.
 .
 This package provides the shared libraries required to run programs 
 compiled with QuantLib.

Package: libquantlib0-dev
Architecture: any
Section: libdevel
Replaces: libquantlib0, libquantlib-0.3.9
Depends: ${shlibs:Depends}, ${misc:Depends}, libc6-dev, libboost-test-dev, libquantlib0v5 (= ${binary:Version})
Description: Quantitative Finance Library -- development package
 The QuantLib project aims to provide a comprehensive software framework 
 for quantitative finance. The goal is to provide a standard free/open 
 source library to quantitative analysts and developers for modeling, 
 trading, and risk management of financial assets.
 .
 This package contains the header files, static libraries and symbolic 
 links that developers using QuantLib will need.

Package: quantlib-examples
Architecture: any
Section: devel
Depends: ${shlibs:Depends}, ${misc:Depends}, libquantlib0v5 (= ${binary:Version})
Breaks: libquantlib0v5 (<< 1.17-1)
Replaces: libquantlib0v5 (<< 1.17-1)
Description: Quantitative Finance Library -- example binaries
 The QuantLib project aims to provide a comprehensive software framework 
 for quantitative finance. The goal is to provide a standard free/open 
 source library to quantitative analysts and developers for modeling, 
 trading, and risk management of financial assets.
 .
 This package provides several example binaries as well as source code.

