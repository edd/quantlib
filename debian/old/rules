#!/usr/bin/make -f
#                                                       -*- makefile -*-
# debian/rules file for the Debian/GNU Linux quantlib package
# Copyright (C) 2001, 2002 by Dirk Eddelbuettel <edd@debian.org>

source  := $(shell head -1 debian/changelog | awk '{print $$1}')
sonum   := 0
libpkg  := lib$(source)$(sonum)
devpkg  := $(libpkg)-dev
smppkg  := $(source)-examples
debtmp	:= $(CURDIR)/debian/$(libpkg)
libdoc	:= $(CURDIR)/debian/$(libpkg)/usr/share/doc/$(libpkg)
arch 	:= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)

export DH_COMPAT=3

# edd 29 Nov 2001	Default to g++
cxx	= g++
## edd 18 May 2002	standard flags for libraries
compilerflags   = -O2 -D_REENTRANT

# 			but use g++-3.0 on ia64
ifneq "$(findstring $(arch), ia64-linux)" ""
cxx	= g++-3.0
endif

## edd 18 May 2002	no optimisation or debugging on baby systems
ifneq "$(findstring $(arch), m68k-linux arm-linux)" ""
compilerflags   = -O0 -g0 -D_REENTRANT
endif

## edd 22 Dec 2002  g++-3.2 for s390
ifneq "$(findstring $(arch), s390-linux)" ""
cxx	= g++-3.2
endif


upstream: get-orig-source	
get-orig-source: 
        #lynx http://quantlib.org/download.html
	lynx http://prdownloads.sf.net/quantlib

build: build-stamp
build-stamp:
	dh_testdir
        #./autogen.sh
	CXX=$(cxx) 						\
	CXXFLAGS="$(compilerflags)"				\
	CFLAGS="$(compilerflags)"				\
	./configure 	--prefix=/usr 				\
			--enable-shared 			\
			--enable-static 			\
			--with-gnu-ld 				\
			--mandir=$(debtmp)/usr/share/man 	\
			--host $(arch)
        # patch libtool re rpath (see lintian docu)
	sed < libtool > libtool-2 \
			-e 's/^hardcode_libdir_flag_spec.*$$/hardcode_libdir_flag_spec=" -D__LIBTOOL_IS_A_FOOL__ "/' \
			-e '/^archive_cmds="/s/"$$/ \\$$deplibs"/'
	mv libtool-2 libtool
	chmod 755 libtool            
        # end libtool rpath patch
	$(MAKE) 	CFLAGS="$(compilerflags)"  	\
			CXXFLAGS="$(compilerflags)"  	\
			CXX=$(cxx)
	touch build-stamp

test:	test-stamp
test-stamp: build-stamp
	-$(MAKE) check
	touch test-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp install-stamp test-stamp 
	rm -rf debian/static/
	dh_clean lib/*so* build/*.so*
	-$(MAKE) distclean
	-rm -rf config.log config.status

install: install-stamp 
install-stamp: 
	$(MAKE) -f debian/rules DH_OPTIONS= install-work
install-work:
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs
	$(MAKE) prefix=$(debtmp)/usr	 	install
        # clean usr/aclocal
	rm -rvf 	$(debtmp)/usr/aclocal
	dh_installexamples	-p$(libpkg)	Examples/*
	(cd $(libdoc)/examples; rm -vf Makefile */Makefile */*.o; \
				rm -rvf */.deps */.libs) || true
	dh_installdirs 	-p$(smppkg) 	usr/share/man/man1
	dh_installman	-p$(smppkg)	debian/BermudanSwaption.1
        # now part of upstream: debian/DiscreteHedging.1 \
        #                       debian/quantlib-config.1 \
        # defunct		debian/Parities.1 	  
        # move static libs into dev-package
	dh_movefiles 	-p$(devpkg) --sourcedir=debian/$(libpkg)
        # move example files into smp-package
	dh_movefiles 	-p$(smppkg) --sourcedir=debian/$(libpkg)
        # some cleanup after movefiles
	-rm -vf  	$(debtmp)/usr/lib/lib*.la
	-rm -rvf 	$(debtmp)/usr/include
	-rm -rvf	$(debtmp)/usr/share/doc/$(libpkg)/examples
        # install lintian "silencer"
	dh_installdirs -p$(libpkg) usr/share/lintian/overrides
	install -p -m 0644  debian/$(libpkg).lintian \
			$(debtmp)/usr/share/lintian/overrides/$(libpkg)
	touch install-stamp

# This single target is used to build all the packages, all at once, or
# one at a time. So keep in mind: any options passed to commands here will
# affect _all_ packages. Anything you want to only affect one package
# should be put in another target, such as the install target.
binary-common: build test install 
	dh_testdir
	dh_testroot
#	dh_installdebconf
	dh_installdocs		-p$(libpkg)	Readme.txt TODO.txt \
						Authors.txt Contributors.txt \
						History.txt TODO.txt \
						News.txt quantlib.m4
	dh_link
#	dh_installmenu
#	dh_installemacsen
#	dh_installpam
#	dh_installinit
#	dh_installcron
#	dh_installinfo		
#	dh_undocumented		
	dh_installchangelogs	ChangeLog.txt
	dh_strip		-N$(devpkg)
	dh_link
	dh_compress
	dh_fixperms
#	dh_suidregister
	dh_makeshlibs
	dh_installdeb
#	dh_perl
	dh_shlibdeps
	dh_gencontrol
#	dh_md5sums
	dh_builddeb


# Build architecture independant packages using the common target.
binary-indep: build install
# (Uncomment this next line if you have such packages.)
#	 $(MAKE) -f debian/rules DH_OPTIONS=-i binary-common

# Build architecture dependant packages using the common target.
binary-arch: build install
	$(MAKE) -f debian/rules DH_OPTIONS=-a binary-common

# Any other binary targets build just one binary package at a time.
binary-%: build install
	make -f debian/rules binary-common DH_OPTIONS=-p$*

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install test
